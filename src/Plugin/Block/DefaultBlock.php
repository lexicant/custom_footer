<?php

/**
 * @file
 * Contains Drupal\custom_footer\Plugin\Block\DefaultBlock.
 */

namespace Drupal\custom_footer\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'DefaultBlock' block.
 *
 * @Block(
 *  id = "default_block",
 *  admin_label = @Translation("Default Block")
 * )
 */
class DefaultBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'custom_markup_string' => t('A default value. This is a block'),
      'second_value' => 12,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['custom_markup_string_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom Markup'),
      '#description' => $this->t('somethings'),
      '#default_value' => $this->configuration['custom_markup_string'],
    ];
    $form['second_config'] = [
      '#type' => 'number',
      '#title' => 'a second config',
      '#default_value' => $this->configuration['second_value'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['custom_markup_string']
      = $form_state->getValue('custom_markup_string_text');
    $this->configuration['second_value']
      = $form_state->getValue('second_config');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'markup',
      '#markup' => $this->configuration['custom_markup_string'] . 'but lets not forget' . $this->configuration['second_value'],
    ];
  }
}
