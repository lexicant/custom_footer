<?php

/**
 * @file
 * Contains Drupal\custom_footer\Controller\DefaultController.
 */

namespace Drupal\custom_footer\Controller;

use Drupal\Core\Controller\ControllerBase;

class DefaultController extends ControllerBase {

  /**
   * Hello.
   *
   * @
   *
   * @return string
   *   Return Hello string.
   */
  public function hello($name) {
    return "Hello " . $name . " !";
  }
}
